import express, { json } from 'express';
import axios from 'axios';
import cors from 'cors';
import request from 'request';
import bodyParser from 'body-parser';


const port = 5000;
const url = 'https://api.spacexdata.com/v4/launches';
const url2 = 'https://api.spacexdata.com/v4/launchpads';
const app = express();

app.use(cors(), bodyParser.json());




app.get('/launches', (req, res)=>{
  
        request(url, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                
                const launches_json = JSON.parse(body);
                const data = launches_json.map((info)=>{
                        return {
                         id: info.id,
                         name: info.name,
                         date: info.date_utc,
                         success: info.success,
                         details: info.details,
                         launchpad: info.launchpad
                        }
                        });
                   
                     res.send(data);
                }
 
            })
    
    
    
      
    } )

    app.get('/launchpads', (req, res)=>{
    
            request(url2, (error, response, body) => {
                if (!error && response.statusCode === 200) {
                    
                    const launches_json = JSON.parse(body);
                    const data = launches_json.map((info)=>{
                            return {
                             id: info.id,
                             name: info.full_name
                            }
                            });
                       
                         res.send(data);
                    }
     
                })
        
        
        
          
        } )
    


app.listen(port, () => console.log(`Listening on port ${port}`));