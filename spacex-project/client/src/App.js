import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import HomePage from './components/HomePage';
import DetailedInfo from './components/DetailedInfo/DetailedInfo';

function App() {

  return (
    <div className='main'>
      <BrowserRouter>
                <Switch>
                    <Route path="/search">
                        <DetailedInfo />
                   </Route>
                    <Route path="/">
                        <HomePage />
                   </Route>
                </Switch>
            </BrowserRouter>

    </div>
  );
}

export default App;
