import React, {useState, useEffect} from 'react';
import axios from 'axios';


const useCustomFetch = () => {
    const [launch, setLaunch] = useState({ launches: [] });
    const [launchpads, setLaunchpads] = useState({ launchpads: [] });
    const [response, setResponse] = useState({ isLoading: true });
    

    const fetchAllData = () => {
        const launchesData = axios.get('/launches');
        const launchpadsData = axios.get('/launchpads');
        setResponse({ isLoading: true });
  
        axios.all([launchesData, launchpadsData]).then(axios.spread((...alldata) => {
            const launchesInfo = alldata[0];
            const launchpadsInfo = alldata[1];
    
            setLaunch(launchesInfo);
            setLaunchpads(launchpadsInfo);
            setResponse({ isLoading: false });
        }));
    }

    useEffect(() => {
        fetchAllData();
    }, [])
   
    return launch;


    
}
export default useCustomFetch;
