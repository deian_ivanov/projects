import React, {useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Pagination from './Pagination';
import { Link } from 'react-router-dom';
import useCustomFetch from '../custom-hook/useCustomFetch';
import useCustomFetchLaunchpads from '../custom-hook/useCustomFetchLaunchpads';
import Loader from '../components/Loader';

const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
        Deyan Ivanov- 
       {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));


const Album = (props) => {
  const classes = useStyles();
  const [currentPage, setCurrentPage] = useState(1);
  const [launchesPerPage] = useState(6); 

  const custom = useCustomFetch();
  const customLaunchpads = useCustomFetchLaunchpads();

  if (!custom || custom.launches || !customLaunchpads || customLaunchpads.launchpads) {
   return  <Loader />
    
  }


  const indexOfLastLaunch = currentPage * launchesPerPage;
  const indexOfFirstLaunch = indexOfLastLaunch - launchesPerPage;
  const currentLaunch = custom.data.slice(indexOfFirstLaunch, indexOfLastLaunch);

  const paginate = pageNumber => setCurrentPage(pageNumber);
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
          SpaceX API
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h2" variant="h2" align="center" color="textPrimary" gutterBottom>
              Browse SpaceX API and be part of the future!
            </Typography>
            <Typography variant="h5" align="center" color="textSecondary" paragraph>
            Space Exploration Technologies Corp. (SpaceX) is an American aerospace manufacturer and space transportation services company headquartered in Hawthorne, California. 
            </Typography>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
                  {/* End hero unit */}
          <Grid container spacing={4}>
                      {currentLaunch.map((launch) => 
         
              <Grid item key={launch.id} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image="https://images.unsplash.com/photo-1541185933-ef5d8ed016c2?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80"
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h4" component="h2">
                    Mission name: {launch.name}
                    </Typography>
                    <Typography gutterBottom variant="h6" component="h2">
                    Launch date: {launch.date}
                    </Typography>
                    <Typography>
                    Launchpad: { customLaunchpads.data.map((launchpad) => {
                     if (launchpad.id === launch.launchpad) {
                       return launchpad.name;
                   }
                })}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" color="primary">
                    <Link to={`/search?${launch.id}`}>View</Link>
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
          )}
                  </Grid>
                  
        </Container>
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
>
        <Pagination launchesPerPage={launchesPerPage}
                              totalLaunches={custom.data.length}
                              paginate={paginate}/>
      </Grid>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="h6" align="center" gutterBottom>
          SpaceX API
        </Typography>
        <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          Developed by Deyan Ivanov. GitLab https://gitlab.com/deian_ivanov/projects.git
        </Typography>
        <Copyright />
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}

export default Album;