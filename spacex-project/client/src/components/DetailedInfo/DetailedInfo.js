import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Typography from './Typography';
import ProductHeroLayout from './Layout';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import useCustomFetch from '../../custom-hook/useCustomFetch';
import { useLocation } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Loader from '../../components/Loader';

const styles = (theme) => ({
  root: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
  },
  images: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexWrap: 'wrap',
  },
  imageWrapper: {
    position: 'relative',
    display: 'block',
    padding: 0,
    borderRadius: 0,
    height: '40vh',
    [theme.breakpoints.down('sm')]: {
      width: '100% !important',
      height: 100,
    },
    '&:hover': {
      zIndex: 1,
    },
    '&:hover $imageBackdrop': {
      opacity: 0.15,
    },
    '&:hover $imageMarked': {
      opacity: 0,
    },
    '&:hover $imageTitle': {
      border: '4px solid currentColor',
    },
  },
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.5,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px 14px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
  
  button: {
    minWidth: 200,
  },
  h5: {
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(10),
    },
  },
  more: {
    marginTop: theme.spacing(2),
  },
});





function ProductCategories(props) {
  const { classes } = props;
  const { search } = useLocation();
  const custom = useCustomFetch();
  

  if (!custom || custom.launches) {
   return  <Loader />
    
  }
 
  return (
    <>
   <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
          SpaceX API
          </Typography>
        </Toolbar>
      </AppBar>
    <ProductHeroLayout >
      {/* Increase the network loading priority of the background image. */}
      {custom.data.filter((info) => {
           return info.id === search.slice(1)
      }).map((launch) => 
        <Grid key={launch.id}>
        <Typography variant="h6" color="inherit" noWrap>
          ID: {launch.id} 
        </Typography>
        <Typography variant="h6" color="inherit" noWrap>
          Name: {launch.name} 
          </Typography>
          <Typography variant="h6" color="inherit" noWrap>
          Date: {launch.date} 
          </Typography>
          <Typography variant="h6" color="inherit">
          Details: {launch.details ? launch.details : 'NO DETAILS'} 
          </Typography>
          <Typography variant="h6" color="inherit" noWrap>
          Outcome: {launch.success === true ? 'SUCCESS' : 'FAILER'} 
        </Typography>
      </Grid>
      
              )}
            
      
      </ProductHeroLayout>
      <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            m={2}
            >
                <Button color="secondary"
                  variant="contained"
                  size="large"
                  className={classes.button}
                >
                <Link to={'/'}>Back</Link> 
                </Button>
      </Grid>
       
      </>
  );
}

ProductCategories.propTypes = {
  classes: PropTypes.object.isRequired,
  };


export default withStyles(styles)(ProductCategories);