# Getting Started with Type Racer App

1. To start the project open a new terminal and write **npm install**;
2. Then write **npm start**;
3. Login with your google account;
4. You should start typing from the first word in the list and proceed with the next one.
5. The timer will stop after all 100 words have been written;
6. Result will be displayed on the screen;
7. You can check you previous results or try again...

Enjoy!!!
