import React, {useState, useEffect} from 'react';
import { NavLink } from 'react-router-dom';
import Logout from './LogOut';

const PlayAgain = () => {
  const [testResults, setTestResults] = useState([]);
  const results = JSON.parse(localStorage.getItem('data'));

  useEffect(() => {
        
    setTestResults(results);

}, [setTestResults])

  
  const lastResult = testResults[testResults.length - 1];
 
  if (!lastResult) {
    return (
      <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    )
}
  return (
    <>
      <div className="container">
        <div className="d-flex justify-content-between mt-3">
          <p className="font-weight-bold">Type Racer</p>
          <NavLink to="/results">
            <p className="text-dark font-weight-bold">
              <u>Previous Games</u>
            </p>
          </NavLink>
        </div>

        <div className="text-center mt-5">
          <span className="timer text-success">
            {lastResult.time.m >= 10
              ? lastResult.time.m
              : "0" + lastResult.time.m}
          </span>
          &nbsp;<span className="timer text-success">:</span>&nbsp;
          <span className="timer text-success">
            {lastResult.time.s >= 10
              ? lastResult.time.s
              : "0" + lastResult.time.s}
          </span>
          &nbsp;<span className="timer text-success">:</span>&nbsp;
          <span className="timer text-success">
            {(lastResult.time.ms / 100).toFixed(2).split(".")[1]}
          </span>
          <div className="text-center mt-5">
            <NavLink to="/">
              <button className="btn btn-success">Try Again?</button>
            </NavLink>
          </div>
          <div className="text-center mt-5 mb-5">
            <span className="mr-5">
              <span className="font-weight-bold">WPM:</span>{" "}
              {Math.floor(lastResult.wpm || 0)}
            </span>
            <span>
              <span className="font-weight-bold">CPM:</span>{" "}
              {Math.floor(lastResult.cpm || 0)}
            </span>
          </div>
          <Logout />
        </div>
      </div>
    </>
  );
	
}

export default PlayAgain;