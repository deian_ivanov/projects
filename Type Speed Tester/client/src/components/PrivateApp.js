import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React from "react";
import HomePage from "./HomePage";
import TestResults from "./TestResults";
import PlayAgain from "./PlayAgain";

const PrivateApp = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route exact path="/results">
            <TestResults />
          </Route>
          <Route exact path="/newgame">
            <PlayAgain />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default PrivateApp;
