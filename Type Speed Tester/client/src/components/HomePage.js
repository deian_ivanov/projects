import React, { useState, useEffect } from "react";
import Timer from "./Timer";
import Word from "./Word";
import "./HomePageStyle.css";
import { NavLink, withRouter } from "react-router-dom";
import Logout from "./LogOut";
import randomwords from "random-words";

const HomePage = (props) => {
  const [userInput, setUserInput] = useState("");
  const [startCounting, setStartCounting] = useState(false);
  const [words, setWords] = useState([]);
  const [activeWordIndex, setActiveWordIndex] = useState(0);
  const [correctWordArray, setCorrectWordArray] = useState([]);
  const [keyPress, setKeyPress] = useState(0);
  const [time, setTime] = useState({ ms: 0, s: 0, m: 0, h: 0 });
  const [interv, setInterv] = useState();

  useEffect(() => {
    if (startCounting) {
      run();
      setInterv(setInterval(run, 10));
    } 
  }, [startCounting]);

  useEffect(() => {
    setWords(randomwords(100));
  }, [setWords]);

  let updatedMs = time.ms,
    updatedS = time.s,
    updatedM = time.m,
    updatedH = time.h;

  const wpm = correctWordArray.filter(Boolean).length / (time.s / 60);
  const cpm = keyPress / (time.s / 60);

  const run = () => {
    if (updatedM === 60) {
      updatedH++;
      updatedM = 0;
    }
    if (updatedS === 60) {
      updatedM++;
      updatedS = 0;
    }
    if (updatedMs === 100) {
      updatedS++;
      updatedMs = 0;
    }
    updatedMs++;
    return setTime({ ms: updatedMs, s: updatedS, m: updatedM, h: updatedH });
  };

  const processInput = (value) => {
    if (activeWordIndex === words.length) {
      // stop
      return;
    }

    if (!startCounting) {
      setStartCounting(true);
    }

    if (value.endsWith(" ")) {
      // the user has finished this word

      if (activeWordIndex === words.length - 1) {
        // overflow
        setInterv(clearInterval(interv));
        setStartCounting(false);
        //adding data to localstorage

        const testResults = (() => {
          const fieldValue = localStorage.getItem("data");
          return fieldValue === null ? [] : JSON.parse(fieldValue);
        })();

        testResults.push({
          wpm: Math.floor(wpm),
          cpm: Math.floor(cpm),
          date: new Date().toLocaleString(),
          time: time,
        });
        localStorage.setItem("data", JSON.stringify(testResults));

        props.history.push("./newgame");
      } else {
        setUserInput("");
      }
      setActiveWordIndex((index) => index + 1);

      // correct word
      setCorrectWordArray((data) => {
        const word = value.trim();
        const newResult = [...data];
        newResult[activeWordIndex] = word === words[activeWordIndex];
        return newResult;
      });
    } else {
      setUserInput(value);
    }
  };

  return (
    <>
      <div className="container">
        <div className="d-flex justify-content-between">
          <p className="font-weight-bold">Type Racer</p>
          <NavLink to="/results">
            <p className="text-dark font-weight-bold">
              <u>Previous Games</u>
            </p>
          </NavLink>
        </div>

        <Timer time={time} />

        <p className="text text-center p-3">
          {words.map((word, index) => {
            return (
              <Word
                key={index}
                text={word}
                active={word === userInput}
                correct={correctWordArray[index]}
              />
            );
          })}
        </p>
        <input
          className="text-center"
          placeholder="Start typing here..."
          type="text"
          value={userInput}
          onChange={(e) => processInput(e.target.value)}
          onKeyPress={() => setKeyPress((count) => count + 1)}
        />
        <div className="text-center mt-3">
          <span className="mr-5">
            <span className="font-weight-bold">WPM:</span>{" "}
            {Math.floor(wpm || 0)}
          </span>
          <span>
            <span className="font-weight-bold">CPM:</span>{" "}
            {Math.floor(cpm || 0)}
          </span>
        </div>
        <div className="text-center mt-5">
          <Logout />
        </div>
      </div>
    </>
  );
};

export default withRouter(HomePage);
