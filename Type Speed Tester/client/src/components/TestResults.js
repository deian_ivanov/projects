import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./HomePageStyle.css";
import Logout from "./LogOut";
import Pagination from "./Pagination";

const TestResults = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [gamesPerPage] = useState(5);

  const testResults = JSON.parse(localStorage.getItem("data"));

  if (!testResults) {
    return (
      <>
      <div className="d-flex justify-content-center">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
        <div className="text-center mt-3"><span>There are no results to display...</span></div>
        </>
    )
}
  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGame = testResults.slice(indexOfFirstGame, indexOfLastGame);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <>
      <div className="container">
        <div className="d-flex justify-content-between">
          <p className="font-weight-bold">Type Racer</p>
          <p className="text-dark font-weight-bold">
            <u>Previous Games</u>
          </p>
        </div>
        <div className="d-flex justify-content-between mt-5">
          <div>
            <p>Your games:</p>
          </div>
          <div className="btn mb-2">
            <NavLink to="/">Back</NavLink>
          </div>
        </div>
        <div>
          {currentGame.map((result, index) => {
            return (
              <div key={index} className="statistic mb-3">
                <p className="font-weight-bold m-2">
                  WPM: {JSON.stringify(result.wpm)}
                </p>
                <p className="font-weight-bold m-2">
                  CPM: {JSON.stringify(result.cpm)}
                </p>
                <p className="m-2">Date: {JSON.stringify(result.date)}</p>
              </div>
            );
          })}
        </div>
        <div>
          <Pagination
            gamesPerPage={gamesPerPage}
            totalGames={testResults.length}
            paginate={paginate}
          />
        </div>
        <div className="d-flex justify-content-center mt-5">
          <Logout />
        </div>
      </div>
    </>
  );
};

export default TestResults;
