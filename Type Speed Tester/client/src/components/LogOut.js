import React from 'react';
import { GoogleLogout } from 'react-google-login';
import LoginContext from './LoginContext';

const Logout = () => {
    const loged = React.useContext(LoginContext);

    return (
            <GoogleLogout
			className="text-danger font-weight-bold"
            clientId="737201474813-sihjd52k28a4d9gpoii4sqkonl9vfemm.apps.googleusercontent.com"
            buttonText="Log Out"
            onLogoutSuccess={loged}
            icon={false} >
			</GoogleLogout>

    )

}


export default Logout;