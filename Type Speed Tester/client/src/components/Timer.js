import React from "react";
import "./HomePageStyle.css";

const Timer = (props) => {
  const { time } = props;

  const hours = () => {
    if (time.h === 0) {
      return "";
    } else {
      return <span>{time.h >= 10 ? time.h : "0" + time.h}</span>;
    }
  };

  return (
    <div>
      {hours()}&nbsp;&nbsp;
      <div className="text-center">
        <span className="timer">{time.m >= 10 ? time.m : time.m}</span>&nbsp;
        <span className="timer">:</span>&nbsp;
        <span className="timer">{time.s >= 10 ? time.s : "0" + time.s}</span>
        &nbsp;<span className="timer">:</span>&nbsp;
        <span className="timer">
          {(time.ms / 100).toFixed(2).split(".")[1]}
        </span>
      </div>
    </div>
  );
};

export default Timer;
