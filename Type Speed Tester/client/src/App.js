import React, { useState, useEffect } from 'react';
import './App.css';
import { GoogleLogin } from 'react-google-login';
import PrivateApp from './components/PrivateApp';
import LoginContext from './components/LoginContext';

const App = () => {
  const [isLogedIn, setIsLogedIn] = useState(false);
  const [user, setUser] = useState({name: '', email: ''})

  const responseGoogle = async (response) => {
    try {
      const userInfo = await response;
      console.log(userInfo);
      setUser({
          name: userInfo.profileObj.name,
        email: userInfo.profileObj.email
      })
    } catch (err) {
      console.log(err)
     }
  }
  
  useEffect(() => {
      if (user.name.length > 0 || user.email.length > 0) {
           setIsLogedIn(true)
      }
     
  }, [user])

  return (
<>

    <div className="container">
      <div className="row h-100">
          <div className="col-sm-12 my-auto">
          <LoginContext.Provider value={ setIsLogedIn }>
                 {isLogedIn ? <PrivateApp /> : <div className="main">  
            <div className="row w-100">
              <div className="col-sm-12 text-center">
                <h2 className="font-weight-bold">Welcome to Type Racer</h2>
              </div>
            </div>
            <div className="row w-100">
              <div className="col-sm-12 text-center">
                  <GoogleLogin
                    clientId= '737201474813-sihjd52k28a4d9gpoii4sqkonl9vfemm.apps.googleusercontent.com'
                    buttonText="Login with Google"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                    isSignedIn={true}
                    cookiePolicy={'single_host_origin'}
                />
                </div>
            </div>
          </div>
              }
              </LoginContext.Provider>
    </div>
      </div>
      </div>
      

      </>
  )
}

export default App;
