

    let count = 0;
    let balanceCount = 0;
    
 const spinTheWheel = () => {
    const x = 1000; 
    const y = 9999; 
    const prizeMessage = document.getElementById('message');
   
    count++;
    
    if(((count % 10) % 4 === 1)){
        let deg = 1990;
        document.getElementById('box').style.transform = `rotate(${deg}deg)`;
        checkPrize(deg, prizeMessage);
    }else if((count % 10) % 4 === 3){
        let deg = 5990;
        document.getElementById('box').style.transform = `rotate(${deg}deg)`;
        checkPrize(deg, prizeMessage);
        
    }else{
        let deg = Math.floor(Math.random() * (x - y)) + y;
        document.getElementById('box').style.transform = `rotate(${deg}deg)`;
        checkPrize(deg, prizeMessage);
    }
	
    
}

const checkPrize = (deg, prizeMessage) =>{
    const balance = document.getElementById('balance');
    const laps = deg / 360;
    const movement = Math.floor((360 - (Math.floor((laps % 1) * 100) * 3.6)) / 22.5);
    const element = document.getElementById('mainbox');  
  	element.classList.remove('animate');
    element.classList.remove('pyro');
	  document.getElementById('btn').disabled = true;

  const setWin = (prize, message) => {
         setTimeout(()=>{
              prizeMessage.style.visibility = 'visible';
              prizeMessage.innerHTML = message;
              balance.innerHTML = `Current balance: ${balanceCount += prize} USD`;
              document.getElementById('btn').disabled = false;
              element.classList.add('animate');
              element.classList.add('pyro');
          }, 5000)
  
          setTimeout(()=>{
              prizeMessage.style.visibility = 'hidden';
          }, 10000)


      }

      switch(movement) {
        case 2:
          setWin(700, 'You have won 700 USD');
          break;
        case 5:
          setWin(10, 'You have won 10 USD');
          break;
        case 3:
          setWin(25, 'BONUS: 25 USD added to your balance!');
          break;
        case 13:
          setWin(25, 'BONUS: 25 USD added to your balance!');
          break;
        case 7:
          setWin(250, 'You have won 250 USD');
          break;
        case 9:
          setWin(310, 'You have won 310 USD');
          break;
        case 11:
          setWin(50, 'You have won 50 USD');
          break;
        case 14:
          setWin(110, 'You have won 110 USD');
          break;
        default:
          setTimeout(()=>{
                    prizeMessage.style.visibility = 'visible';
                    prizeMessage.innerHTML = 'TRY AGAIN!';
                    document.getElementById("btn").disabled = false;
                }, 5000) 
        
                setTimeout(()=>{
                    prizeMessage.style.visibility = 'hidden';
                }, 10000)
      } 
}

