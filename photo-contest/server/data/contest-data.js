import pool from './pool.js';

const getAll = async () => {
  const sql = `
        SELECT id, title, category, description, is_open, image,
        DATE_FORMAT(date_1, "%M %e %Y") as date_1,
        DATE_FORMAT(date_2, "%M %e %Y") as date_2,
        DATE_FORMAT(date_3, "%M %e %Y") as date_3,
        rating
        FROM contest
    `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
        SELECT id, title, category, description, is_open, image,
        DATE_FORMAT(date_1, "%M %e %Y") as date_1, 
        DATE_FORMAT(date_2, "%M %e %Y") as date_2,
        DATE_FORMAT(date_3, "%M %e %Y") as date_3,
        rating
        FROM contest
        WHERE ${column} = ?
    `;
  const result = await pool.query(sql, [value]);

  return result[0];
};

const searchBy = async (column, value) => {
  const sql = `
        SELECT id, title, category, description, is_open, image,
        DATE_FORMAT(date_1, "%M %e %Y") as date_1,
        DATE_FORMAT(date_2, "%M %e %Y") as date_2,
        DATE_FORMAT(date_3, "%M %e %Y") as date_3
        FROM contest
        WHERE ${column} LIKE '%${value}%'
    `;

  return await pool.query(sql);
};

const getByPhase1 = async () => {
  const sql = `
  SELECT id, title, category, description, is_open, image,
  DATE_FORMAT(date_1, "%M %e %Y") as date_1,
  DATE_FORMAT(date_2, "%M %e %Y") as date_2,
  DATE_FORMAT(date_3, "%M %e %Y") as date_3
  FROM contest
  WHERE CURRENT_DATE <= date_1;
    `;

  return await pool.query(sql);
};

const getByPhase2 = async () => {
  const sql = `
  SELECT id, title, category, description, is_open, image,
  DATE_FORMAT(date_1, "%M %e %Y") as date_1,
  DATE_FORMAT(date_2, "%M %e %Y") as date_2,
  DATE_FORMAT(date_3, "%M %e %Y") as date_3
  FROM contest
  WHERE date_1 < CURRENT_DATE AND CURRENT_DATE <= date_2;
    `;

  return await pool.query(sql);
};
const getByPhase3 = async () => {
  const sql = `
  SELECT id, title, category, description, is_open, image,
  DATE_FORMAT(date_1, "%M %e %Y") as date_1,
  DATE_FORMAT(date_2, "%M %e %Y") as date_2,
  DATE_FORMAT(date_3, "%M %e %Y") as date_3
  FROM contest
  WHERE date_2 < CURRENT_DATE AND CURRENT_DATE <= date_3;
    `;

  return await pool.query(sql);
};

const createContest = async (title, category, description, is_open, date_1, date_2, date_3) => {
  // const {title, category, description, is_open, date_1, date_2, date_3 } = newContest
  const sql = `
    INSERT INTO contest(title, category, description, is_open, date_1, date_2, date_3)
    VALUES ( '${title}', '${category}', '${description}', '${is_open}', '${date_1}', '${date_2}', '${date_3}')
    `;
  const result = await pool.query(sql);
  return {
    id: result.insertId,
    title, category, description, is_open, date_1, date_2, date_3
  }
};

const updatePicture = async (id, path) => {
  const sql = `
      UPDATE contest SET
      image = ?
      WHERE id = ?
  `;
  return await pool.query(sql, [path, id]);
};

const removeContest = async (id) => {
  const sql = `
    DELETE
    FROM contest
    WHERE id= ${id}
    `;

  return await pool.query(sql);
};

const updateContest = async (contest) => {
  const { description, is_open, date_1, date_2, date_3, id } = contest;

  const sql = `
      UPDATE contest
      SET description = ?,
          is_open = ?,
          date_1 = ?,
          date_2 = ?,
          date_3 = ?
      WHERE id = ?
    `;

  return await pool.query(sql, [description, is_open, date_1, date_2, date_3, id]);
};

const createPhoto = async (title, comment, is_fit, score, contest_id, user_id) => {
  const sql = `
    INSERT INTO photo (title, category, comment, is_fit, score, contest_id, user_id)
    VALUES ( '${title}', (SELECT category FROM contest WHERE id = '${contest_id}'), '${comment}', '${is_fit}', '${score}', 
    '${contest_id}', '${user_id}')
    `;
  const result = await pool.query(sql);
  return {
    id: result.insertId,
    title, comment, is_fit, score, contest_id, user_id
  }
};

const updatePhoto = async (id, path) => {
  const sql = `
      UPDATE photo SET
      image = ?
      WHERE id = ?
  `;
  return await pool.query(sql, [path, id]);
};

const getAllPhotos = async () => {
  const sql = `
        SELECT *
        FROM photo
    `;

  return await pool.query(sql);
};

const getRating = async (column, value) => {
  const sql = `
      SELECT rating
      FROM contest
      WHERE ${column} = ?
  `;

  const result = await pool.query(sql, [value]);

  return result[0];
}

const updateRating = async (contest) => {
  const {
      id,
      rating
  } = contest;
  const sql = `
      UPDATE contest SET
        rating = ?
      WHERE id = ?
  `;

  return await pool.query(sql, [rating, id]);
}

export default {
  getAll,
  searchBy,
  getBy,
  getByPhase1,
  getByPhase2,
  getByPhase3,
  createContest,
  updatePicture,
  removeContest,
  updateContest,
  createPhoto,
  updatePhoto,
  getAllPhotos,
  getRating,
  updateRating
};
