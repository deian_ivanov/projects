import pool from './pool.js';

const getAll = async () => {
    const sql = `
        SELECT id, username, roleid, avatar, createTime, score, first_name, second_name, email
        FROM user
        
    `;

    return await pool.query(sql);
};

const getAllOrdered = async () => {
    const sql = `
        SELECT *
        FROM user
        ORDER BY score ASC;
    `;

    return await pool.query(sql);
};

const getWithRole = async (username) => {
    const sql = `
        SELECT u.id, u.username, u.password, r.name as role
        FROM user u
        JOIN role r ON u.roleId = r.id
        WHERE u.username = ?
    `;

    const result = await pool.query(sql, [username]);

    return result[0];
}

const getBy = async (column, value) => {
    const sql = `
        SELECT id, username, roleid, avatar, createTime, score, first_name, second_name, email
        FROM user
        WHERE ${column} = ?
    `;

    const result = await pool.query(sql, [value]);

    return result[0];
}

const searchBy = async (column, value) => {
    const sql = `
        SELECT id, username 
        FROM user
        WHERE ${column} LIKE '%${value}%' 
    `;

    return await pool.query(sql);
}

const create = async (username, firstName, secondName, password, email, createTime, role) => {
    const sql = `
        INSERT INTO user(username, first_name, second_name, password, email, createTime, roleid)
        VALUES (?,?,?,?,?,?,(SELECT id FROM role WHERE name = ?))
    `;

    const result = await pool.query(sql, [username, firstName, secondName, password, email, createTime, role]);

    return {
        id: result.insertId,
        username: username,
        firstName: firstName,
        secondName: secondName,
        email: email,
        createTime: createTime
    };
}

const update = async (user) => {
    const {
        id,
        username
    } = user;
    const sql = `
        UPDATE user SET
          username = ?
        WHERE id = ?
    `;

    return await pool.query(sql, [username, id]);
}

const remove = async (user) => {
    const sql = `
        DELETE FROM user 
        WHERE id = ?
    `;

    return await pool.query(sql, [user.id]);
}

const updateAvatar = async (id, path) => {
    const sql = `
        UPDATE user SET
        avatar = ?
        WHERE id = ?
    `;
    return await pool.query(sql, [path, id]);
  };

export default {
    getAll,
    getAllOrdered,
    searchBy,
    getBy,
    create,
    update,
    remove,
    getWithRole,
    updateAvatar
}