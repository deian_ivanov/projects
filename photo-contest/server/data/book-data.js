import pool from './pool.js';

const getAll = async () => {
    const sql = `
        SELECT id, title, category, comment
        FROM photo
    `;

    return await pool.query(sql);
};

const getBy = async (column, value) => {
    const sql = `
        SELECT id, title, author, description, isAvailable, coverPath
        FROM books
        WHERE ${column} = ?
    `;  

    const result = await pool.query(sql, [value]);

    return result[0];
};

const searchBy = async (column, value) => {
    const sql = `
        SELECT id, title, author, description
        FROM books
        WHERE ${column} LIKE '%${value}%' 
    `; 

    return await pool.query(sql);
};

// const updateAvatar = async (id, path) => {
//   const sql = `
//       UPDATE books SET
//       coverPath = ?
//       WHERE id = ?
//   `;
//   return await pool.query(sql, [path, id]);
// };

const updateBook = async (id) => {
    const sql = `
      UPDATE books
      SET isAvailable = ?
      WHERE id = ?
    `;
  
    return await pool.query(sql, [0, id]);
  };
  
  const returnBook = async (id) => {
    const sql = `
      UPDATE books
      SET isAvailable = ?
      WHERE id = ?
    `;
  
    return await pool.query(sql, [1, id]);
  };
  const viewReview = async (id) => {
    const sql =`
    SELECT distinct r.review, r.id
    FROM books b
    JOIN reviews r
    ON r.books_id = ?
    WHERE books_id = ? and r.isDeleted = ?
    `;
return await pool.query(sql, [id, id, 1]);
};


  const addReview = async (review, books_id, user_id) => {
    const sql = `
    INSERT INTO reviews (books_id, review, users_id)
    VALUES (?, ?, ?)
    `;
  
    return await pool.query(sql, [review, books_id, user_id]);
  };

  const removeReview = async (id, userid, reviewid) => {
    const sql = `
      UPDATE reviews
      SET isDeleted = ?
      WHERE id = ? AND users_id = ?
    `;
  
    return await pool.query(sql, [0, reviewid, userid]);
  };
  const updateReview = async (id, review, reviewid, userid) => {
    const sql = `
      UPDATE reviews
      SET review = ?
      WHERE id = ? AND users_id = ? AND isDeleted = ?
    `;
  
    return await pool.query(sql, [review, reviewid, userid, 1]);
  };

  const getUser = async (reviewid) => {
    const sql = `
      SELECT users_id, isDeleted
      FROM reviews
      WHERE id = ?
    `;
  
    return await pool.query(sql, [reviewid]);
  };

  const getPage = async (limit, offset) => {
    const sql = `
        SELECT id, title, description
        FROM books 
        LIMIT ? OFFSET ?;
    `;

    return await pool.query(sql, [limit, offset]);
};

const getJobsCount = async () => {
    const sql = `
        SELECT COUNT(*) as count FROM books 
    `;

    return await pool.query(sql);
};
export default {
    getAll,
    searchBy,
    getBy,
    returnBook,
    updateBook,
    viewReview,
    addReview,
    removeReview,
    updateReview,
    getUser,
  // updateAvatar,
  getPage,
  getJobsCount
   
};
