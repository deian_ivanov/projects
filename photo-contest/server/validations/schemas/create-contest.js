export const createContestSchema = {
    title: value => {
        if (!value) {
            return 'Title is required';
        }
        
        if (typeof value !== 'string' || value.length < 2 || value.length > 25) {
            return 'Title should be a string in range [2..25]';
        }

        return null;
    },
    category: value => {
        if (!value) {
            return 'Category is required';
        }

        if (typeof value !== 'string' || value.length < 2 || value.length > 100) {
            return 'Title should be a string in range [2..100]';
        }

        // if (value < 0) {
        //     return 'Age cant be negative';
        // }

        return null;
    },
    description: value => {
        if (!value) {
            return 'Description is required';
        }

        if (typeof value !== 'string' || value.length < 2 || value.length > 100) {
            return 'Description should be a string in range [2..100]';
        }
        return null;
    },

    is_open: value => {
            
    
    },

    date_1: value => {
        if (!value) {
            return 'First phase deadline is required';
        }

        if (isNaN(value)) {
            return 'Date should be entered in format yyyy-mm-dd';
        }

        if (typeof value !== '%yyyy-%mm-%dd') {
            return 'Date should  be in format yyyy-mm-dd';
        }

    },

    date_2: value => {
        if (!value) {
            return 'First phase deadline is required';
        }

        if (isNaN(value)) {
            return 'Date should be entered in format yyyy-mm-dd';
        }

        if (typeof value !== '%yyyy-%mm-%dd') {
            return 'Date should  be in format yyyy-mm-dd';
        }
    },
    
    date_3: value => {
        if (!value) {
            return 'First phase deadline is required';
        }

        if (isNaN(value)) {
            return 'Date should be entered in format yyyy-mm-dd';
        }

        if (typeof value !== '%yyyy-%mm-%dd') {
            return 'Date should  be in format yyyy-mm-dd';
        }
    }
}
