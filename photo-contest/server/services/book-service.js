import bookData from '../data/book-data.js';


const getAllBooks = async (filter) => {
  return filter
    ? await bookData.searchBy('title', filter)
    : await bookData.getAll();
};

const getBookById = async (id) => await bookData.getBy('id', id);



const updateBook = bookData => {

  return async (id, isAvailable) => {
    const book = await getBookById(id);

    if (!book) {
      return;
    }

    await bookData.updateBook(id, isAvailable);

    return `The book with id ${id} has been borrowed!`;
  };
};

const returnBook = bookData => {

  return async (id, isAvailable) => {
    const book = await getBookById(id);

    if (!book) {
      return;
    }

    await bookData.returnBook(id, isAvailable);

    return `The book with id ${id} has been returned!`;
  };
};

const viewReview = bookData => {

  return async (id, review) => {
    const book = await getBookById(id);

    if (!book) {
      return;
    }

   const reviews = await bookData.viewReview(id, review);
   
    return reviews;
   
  };
};


const addReview = bookData => {

  return async (id, review, user_id) => {
    const book = await getBookById(id);

    if (!book) {
      return;
    }

    await bookData.addReview(id, review, user_id);

    return {
      ...book,
      review,
    };
  };
};

const removeReview = bookData => {

  return async (id, userid, reviewid) => {
    const book = await getBookById(id);
   
    if (!book) {
      return;
    }

    await bookData.removeReview(id, userid, reviewid);
 
    return `The review with id ${reviewid} was deleted`;
  };
};

const updateReview = bookData => {

  return async (id, review, reviewid, userid) => {
    const book = await getBookById(id);

    if (!book) {
      return;
    }

    await bookData.updateReview(id, review, reviewid, userid);

    return `The review with ${reviewid} was updated with the following text: (${review}).`;
  };

};

const updateAvatar = bookData => {
  return async (id, path) => { 
      const result = await bookData.updateAvatar(id, path);
     
      return { error: result.affectedRows > 0 ? null : 'error' };
  }
};

const getPage = bookData => {
  return async (pageNumber) => {
      const limit = 5;
      const offset = (pageNumber - 1) * limit;

      const page = await bookData.getPage(limit, offset);     
      const [{ count }] = await bookData.getJobsCount();

      return {
          books: page,
          count: count,
          currentPage: pageNumber,
          hasNext: (offset + limit) < count,
          hasPrevious: pageNumber > 1
      };
  }
}

export default {
  getAllBooks,
  getBookById,
  updateBook,
  returnBook,
  viewReview,
  addReview,
  removeReview,
  updateReview,
  updateAvatar,
  getPage
 
};
