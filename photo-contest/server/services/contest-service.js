import contestData from '../data/contest-data.js';
import serviceErrors from './service-errors.js';


const getAllContests = async (filter) => {
  return filter
    ? await contestData.searchBy('title', filter)
    : await contestData.getAll();
};

const getContestById = async (id) => await contestData.getBy('id', id);

const getPhaseContest = async (date) => {
  switch (date) {
    case 'date_1':
      return await contestData.getByPhase1()
    case 'date_2':
      return await contestData.getByPhase2()
    case 'date_3':
      return await contestData.getByPhase3()
    default:
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        contest: null
      }
  }
}

const createContest = async (createData) => {
  const { title, category, description, is_open, date_1, date_2, date_3 } = createData;
  const existingContest = await contestData.getBy('title', title);
  if (existingContest) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      contest: null
    }
  }
  return {
    error: null,
    contest: await contestData.createContest(title, category, description, is_open, date_1, date_2, date_3)
  }
};

const updatePicture = contestData => {

  return async (id, path) => {
    const result = await contestData.updatePicture(id, path);
    //  return await usersData.updatePicture(id, path);
    return { error: result.affectedRows > 0 ? null : 'error' };
  }
};

const updateContest = async (id, updateData) => {
  const { description, is_open, date_1, date_2, date_3 } = updateData;
  const contest = await contestData.getBy("id", id);
  if (!contest) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      contest: null
    }
  }
  const updatedContest = { ...contest, ...updateData }
  const _ = await contestData.updateContest(updatedContest);
  return {
    error: null,
    contest: updatedContest
  }
};

const deleteContest = async (id) => {
  const contest = await contestData.getBy('id', id);
  if (!contest) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      contest: null
    }
  }
  const _ = await contestData.removeContest(id);
  return {
    error: null,
    contest: contest
  }
};

const createPhoto = async (createData) => {
  const { title, comment, is_fit, score, contest_id, user_id } = createData;
  const existingPhoto = await contestData.getBy('title', title);
  if (existingPhoto) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      photo: null
    }
  }
  return {
    error: null,
    photo: await contestData.createPhoto(title, comment, is_fit, score, contest_id, user_id)
  }
};

const uploadPhoto = contestData => {

  return async (id, path) => {
    const result = await contestData.updatePhoto(id, path);
    return { error: result.affectedRows > 0 ? null : 'error' };
  }
};

const getAllPhotos = async (filter) => {
  return filter
    ? await contestData.searchBy('title', filter)
    : await contestData.getAllPhotos();
};

const getRating = contestData => {
  return async (id) => {
    const rating = await contestData.getRating('id', id);

    if (!rating) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        rating: null
      };
    }

    return {
      error: null,
      rating: rating
    };
  }
};

const updateRating = contestData => {
  return async (id, ratingUpdate) => {
      const rating = await contestData.getBy('id', id);
      if (!rating) {
          return {
              error: 'Error',
              rating: null
          }
      }

      if (ratingUpdate.rating && !!(await contestData.getBy('rating', ratingUpdate.rating))) {
          return {
              error: 'Error',
              rating: null
          }
      }

      const updated = {
          ...rating,
          ...ratingUpdate
      };
      const _ = await contestData.updateRating(updated);

      return {
          error: null,
          rating: updated
      }
  };
}


export default {
  getAllContests,
  getContestById,
  getPhaseContest,
  createContest,
  updatePicture,
  deleteContest,
  updateContest,
  uploadPhoto,
  createPhoto,
  getAllPhotos,
  getRating,
  updateRating
}

