import express from 'express';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import contestService from '../services/contest-service.js';
import contestData from '../data/contest-data.js';
import multer from 'multer';
import storage from './../storage.js';


const contestController = express.Router();

contestController
    // get all contests with searching
    .get('/contests', async (req, res) => {
        const { search } = req.query;
        const contest = await contestService.getAllContests(search);

        res.status(200).send(contest);
    })

    // get contest by id
    .get('/contests/:id', async (req, res) => {
        const { id } = req.params;
        const contest = await contestService.getContestById(+id);
        if (!contest) {
            return res.status(404).send({
                message: 'The contest is not found!',
            });
        }
        res.status(200).send(contest);
    })

    // get phase contest
    .get('/contest_phase', async (req, res) => {
        const { search } = req.query;
        const contest = await contestService.getPhaseContest(search);

        res.status(200).send(contest);
    })

    // create contest
    .post('/contests', authMiddleware, roleMiddleware('Organizer'), async (req, res) => {
        // createValidator(createContestSchema)
        const createData = req.body;
        const contest = await contestService.createContest(createData);
        if (!contest) {
            return res.status(404).json({ msg: `Contest with id ${req.params.id} not found!` });
        }
        res.status(201).json(contest);
    })

    // upload contest's picture
    .post('/contests/:id/picture',
        authMiddleware, roleMiddleware('Organizer'),
        multer({ storage: storage }).single('image'),
        async (req, res,) => {
            const { error } = await contestService.updatePicture(contestData)(req.params.id, req.file.filename);
            if (error) {
                res.sendStatus(500);
            } else {
                res.status(200).send({ path: req.file.filename })
            }
        })

    // update contest
    .put('/contests/:id', authMiddleware, roleMiddleware('Organizer'), async (req, res) => {
        // createValidator(updateContestSchema)
        const { id } = req.params;
        const updateData = req.body;

        const contest = await contestService.updateContest(+id, updateData);
        if (!contest) {
            return res.status(404).json({ msg: `Contest with id ${req.params.id} not found!` });
        }
        res.status(201).json(contest);
    })

    //remove contest by id
    .delete('/contests/:id', authMiddleware, roleMiddleware('Organizer'), async (req, res) => {
        const { id } = req.params;
        const contest = await contestService.deleteContest(+id);
        if (!contest) {
            return res.status(404).json({ msg: `Contest with id ${req.params.id} not found!` });
        }
        res.status(200).json(contest)
    })

    // create photo info
    .post('/contests/:id/photo', authMiddleware, roleMiddleware('Junky'), async (req, res) => {
        const createData = req.body;
        const photo = await contestService.createPhoto(createData);
        if (!photo) {
            return res.status(404).json({ msg: `Photo with id ${req.params.id} not found!` });
        }
        res.status(201).json(photo);
    })

    // upload photo
    .post('/contests/:id/photo/:id',
        authMiddleware, roleMiddleware('Junky'),
        multer({ storage: storage }).single('image'),
        async (req, res,) => {
            const { error } = await contestService.uploadPhoto(contestData)(req.params.id, req.file.filename);
            if (error) {
                res.sendStatus(500);
            } else {
                res.status(200).send({ path: req.file.filename })
            }
        })

        //get all photos
        .get('/photos', async (req, res) => {
            const { search } = req.query;
            const photo = await contestService.getAllPhotos(search);
    
            res.status(200).send(photo);
        })
     
        //get contest rating
        .post('/contest/:id/rating',
     authMiddleware,
     roleMiddleware('Junky'),
     async (req, res) => {
         console.log(res)
         const { id } = req.params;

         const { error, rating } = await contestService.getRating(contestData)(+id);

         if (error) {
             res.status(404).send({ message: 'Contest not found!' });
         } else {
             res.status(200).send(rating);
         }
     })

      // update rating by id
    .put('/contest/:id/rating',
    async (req, res) => {
        const { id } = req.params;
        const updateData = req.body;

        console.log(updateData);

        const { error, rating } = await contestService.updateRating(contestData)(+id, updateData);

        if (error) {
            res.status(404).send({ message: 'Contest not found!' });
        } else if (error) {
            res.status(409).send({ message: 'Name not available' });
        } else {
            res.status(200).send(rating);
        }
    })

export default contestController;
