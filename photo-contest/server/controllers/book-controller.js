import express from 'express';
import { authMiddleware } from '../auth/auth-middleware.js';
import bookService from '../services/book-service.js';
import bookData from '../data/book-data.js';
import multer from 'multer';
import storage from './../storage.js';


const bookController = express.Router();

bookController
    // get all books with searching
    .get('/api/books', async (req, res) => {
        const { search } = req.query;
        const book = await bookService.getAllBooks(search);

        res.status(200).send(book);
    })
    // get books by id
    .get('/api/books/:id', async (req, res) => {
        const { id } = req.params;

        const book = await bookService.getBookById(+id);
        if (!book) {
            return res.status(404).send({
                message: 'The book is not found!',
            });
        }

        res.status(200).send(book);
    })


    // borrow a book

    .post('/api/books/:id', async (req, res) => {
        const book = await bookService.updateBook(bookData)(req.params.id, req.body.isAvailable);
        if (!book) {
            return res.status(404).json({ msg: `Book with id ${req.params.id} not found!`});
        }
        
        res.status(200).json(book);

      })

       //return a book
      .delete('/api/books/:id', async (req, res) => {
        const book = await bookService.returnBook(bookData)(req.params.id, req.body.isAvailable);
        if (!book) {
            return res.status(404).json({ msg: `Book with id ${req.params.id} not found!`});
        }
        
        res.status(200).json(book);

      })
    
    // upload avatar
    // .post('/:id/avatar',
    // authMiddleware,
    // multer({ storage: storage }).single('avatar'),
    // async (req, res,) => {
    //     const { error } = await bookService.updateAvatar(bookData)(
    //         req.params.id,
    //         req.file.filename
    //     );

    //     if (error) {
    //         res.sendStatus(500);
    //     } else {
    //         res.status(200).send({
    //             path: req.file.filename
    //         });
    //     }
    // })
      //view review
    .get('/api/books/:id/reviews', async (req, res) => {
            const { id } = req.params;
    
            const book = await bookService.getBookById(+id);
            
            if (!book) {
                return res.status(404).send({
                    message: 'The book is not found!',
                });
            }
            const review = await bookService.viewReview(bookData)(req.params.id, req.body.review);
            res.status(200).json(review);
        })

    
      
       //add review 
      .post('/api/books/:id/reviews', authMiddleware, async (req, res) => {
        
        const review = await bookService.addReview(bookData)(req.params.id, req.body.review, req.user.id);
        if (!review) {
            return res.status(404).json({ msg: `Book with id ${req.params.id} not found!`});
        }
        
        res.status(201).json(review);

      })

      //update review
      .put('/api/books/:id/reviews/:reviewid', authMiddleware, async (req, res) => {
        
        const review = await bookService.updateReview(bookData)(req.params.id, req.body.review, req.params.reviewid, req.user.id);
        if (!review) {
            return res.status(404).json({ msg: `Book with id ${req.params.id} not found!`});
        }
    
      const checkIfUserMatch = await bookData.getUser(req.params.reviewid);
    
        if(req.user.id !== checkIfUserMatch[0].users_id){
            return res.status(404).json({ msg: `You cannot change review with id ${req.params.reviewid}! It has been written by other user!`});
        }

        if(checkIfUserMatch[0].isDeleted === 0){
            return res.status(404).json({ msg: 'You cannot update this review while it has already been deleted!'});
        }
        res.status(201).json(review);

      })
      
      //remove review

      .delete('/api/books/:id/reviews/:reviewid', authMiddleware, async (req, res) => {

        const checkIfDeleted = await bookData.getUser(req.params.reviewid);

        if(checkIfDeleted[0].isDeleted === 0){
            return res.status(404).json({ msg: 'You cannot delete this review while it has already been deleted!'});
        }

        const book = await bookService.removeReview(bookData)(req.params.id, req.user.id, req.params.reviewid);
        if (!book) {
            return res.status(404).json({ msg: `Book with id ${req.params.id} not found!`});
        }
        
        if(req.user.id !== checkIfDeleted[0].users_id){
            return res.status(404).json({ msg: `You cannot delete review with id ${req.params.reviewid}! It has been written by other user!`});
        }
     
       
        res.status(200).json(book);

      });

export default bookController;
