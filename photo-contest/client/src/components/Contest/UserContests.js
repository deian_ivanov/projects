import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import './Contest.css';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import AppError from '../AppError/AppError';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import { useAuth } from "../../auth/authContext";
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
// import './UserPage.css';



const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

const UserContests = ({ children }) => {
    const classes = useStyles();
    const [photoData, setPhotoData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const { id } = useParams();
    const { user } = useAuth();

    useEffect(() => {
        setLoading(true)
        fetch(`${BASE_URL}/photos`, { mode: 'cors' })
            .then(response => response.json())
            .then(data => {
                if (Array.isArray(data)) {
                    setPhotoData(data);
                } else {
                    throw new Error(data.message);
                }
            })
            .catch(error => setError(error.message))
            .finally(() => setLoading(false));
    }, []);


    if (photoData.isLoading) {
        return (
            <>
                <div className={classes.root}>
                    <Loader />
                </div>
            </>
        )
    }
    // console.log(photoData);
    console.log(user.sub);

    if (error) {
        return (
            <AppError message={error} />
        );
    }

    const userPhotos = photoData.filter((el) => (el.user_id === user.sub))

    return (
        <div className={classes.root}>
            <GridList cellHeight={180} className={classes.gridList}>
                <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                    <ListSubheader component="div"> All my photos {new Date().toDateString()}</ListSubheader>
                </GridListTile>
                {userPhotos.map((photo) =>
                    <GridListTile key={photo.image}>
                        <img src={`http://localhost:3001/public/${photo.image}`} alt={photo.title} />
                        <GridListTileBar
                            title={photo.category.toUpperCase()}
                            subtitle={<span>by: {user.username}</span>}
                            // actionIcon={
                            //     <IconButton aria-label={`info about ${photo.title}`} className={classes.icon}>
                            //         <InfoIcon />
                            //     </IconButton>
                            // }
                        />
                    </GridListTile>
                )}
            </GridList>
        </div>
    );
}


export default UserContests;

// return (
//     <Container className={classes.cardGrid} maxWidth="md">
//         <Grid container spacing={4}>
//             {userPhotos.map((photo) => (
//                 <Grid item key={photo.id} xs={12} sm={6} md={4}>
//                     <Card className={classes.card}>
//                         <CardMedia
//                             className={classes.cardMedia}
//                             // image="http://localhost:3001/public"
//                             // image="https://source.unsplash.com/random"
//                             image={`http://localhost:3001/public/${photo.image}`}
//                             title="Image title"
//                         />
//                         <CardContent className={classes.cardContent}>
//                             <Typography gutterBottom variant="h5" component="h2">
//                                 Heading
//                     </Typography>
//                             <Typography>
//                                 This is a media card. You can use this section to describe the content.
//                     </Typography>
//                         </CardContent>
//                         <CardActions>
//                             <Button size="small" color="primary">
//                                 View
//                     </Button>
//                             <Button size="small" color="primary">
//                                 Edit
//                     </Button>
//                         </CardActions>
//                     </Card>
//                 </Grid>
//             ))}
//         </Grid>
//     </Container>

// )
// }
// export default UserContests;