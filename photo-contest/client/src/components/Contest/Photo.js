import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import './Contest.css';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import AppError from '../AppError/AppError';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import { useParams } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import { useAuth } from "../../auth/authContext";
import useAuthorizedGETRequest from '../../custom-hooks/useAuthorizedFetch';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

const Photo = ({ children }) => {
    const classes = useStyles();
    const [photoData, setPhotoData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const { id } = useParams();
    const { user } = useAuth();

    useEffect(() => {
        setLoading(true)
        fetch(`${BASE_URL}/photos`, { mode: 'cors' })
            .then(response => response.json())
            .then(data => {
                if (Array.isArray(data)) {
                    setPhotoData(data);
                } else {
                    throw new Error(data.message);
                }
            })
            .catch(error => setError(error.message))
            .finally(() => setLoading(false));
    }, []);
 
    //   const users = useAuthorizedGETRequest(`${BASE_URL}/`);

        if (photoData.isLoading) {
            return (
                <>
                    <div className={classes.root}>
                        <Loader />
                    </div>
                </>
            )
        }

        if (error) {
            return (
                <AppError message={error} />
            );
        }
        return (
            <div className={classes.root}>
                <GridList cellHeight={180} className={classes.gridList}>
                    <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                        <ListSubheader component="div">{new Date().toDateString()}</ListSubheader>
                    </GridListTile>
                    {photoData.map((photo) => (
                        <GridListTile key={photo.image}>
                            <img src={`http://localhost:3001/public/${photo.image}`} alt={photo.title} />
                            <GridListTileBar
                                title={photo.title}
                                subtitle={<span>by: {photo.user_id}</span>}
                                actionIcon={
                                    <IconButton aria-label={`info about ${photo.title}`} className={classes.icon}>
                                        <InfoIcon />
                                    </IconButton>
                                }
                            />
                        </GridListTile>
                    ))}
                </GridList>
            </div>
        );
    }


    export default Photo;
