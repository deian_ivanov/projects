import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import './Contest.css';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import AppError from '../AppError/AppError';
import Button from '@material-ui/core/Button';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import { useAuth } from "../../auth/authContext";
import { NavLink } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

const Contest = ({ remove }) => {
    const classes = useStyles();
    const [contestData, setContestData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const { user } = useAuth();

    useEffect(() => {
        setLoading(true)
        fetch(`${BASE_URL}/contests`, { mode: 'cors' })
            .then(response => response.json())
            .then(data => {
                if (Array.isArray(data)) {
                    setContestData(data);
                } else {
                    throw new Error(data.message);
                }
            })
            .catch(error => setError(error.message))
            .finally(() => setLoading(false));
    }, []);
    console.log(contestData);

    const deleteContest = (id) => {
        const token = localStorage.getItem('token');
        setLoading(true);
        fetch(`${BASE_URL}/contests/${id}`, {
            method: 'DELETE',
            headers: { Authorization: `Bearer ${token}` }
        })
            .then(response => response.json())
            .then(() => {
                setContestData(contestData => contestData.filter((contest) => contest.id !== id))
                alert("Contest deleted")
            })
            .catch(error => setError(error.message))
            .finally(() => setLoading(false))
    }

    if (loading) {
        return <Loader />
    }

    if (error) {
        return (
            <AppError message={error} />
        );
    }

    return (
        <div className={classes.root}>
          <GridList cellHeight={180} className={classes.gridList}>
            <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
              <ListSubheader component="div">{new Date().toDateString()}</ListSubheader>
            </GridListTile>
            {contestData.map((contest) => (
              <GridListTile key={contest.image}>
                <img src={`http://localhost:3001/public/${contest.image}`} alt={contest.title}/>
                <GridListTileBar
                  title={<NavLink to={`/contests/${contest.id}`}>{contest.title}</NavLink>}
                  subtitle={<span>by: {user.username}</span>}
                  actionIcon={
                    <Button className={classes.icon} onClick={() => deleteContest(contest.id)}> Delete
                     </Button>
                  }
                />
              </GridListTile>
            ))}
          </GridList>
        </div>
      );
    }


export default Contest;