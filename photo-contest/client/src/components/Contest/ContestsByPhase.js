import React, { useEffect, useState } from 'react';
import ContestDetails from './ContestDetails';
import PropTypes from 'prop-types';
import './Contest.css';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import AppError from '../AppError/AppError';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
}));

const ContestsbyPhase = (props) => {
    const { date } = props;
    const classes = useStyles();
    const [contestData, setContestData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/contest_phase?search=${date}`, { mode: 'cors' })
            .then(response => response.json())
            .then(data => {
                if (Array.isArray(data)) {
                    setContestData(data);
                } else {
                    throw new Error(data.message);
                }
            })
            .catch(error => setError(error.message))
            .finally(() => setLoading(false));
    }, []);

    if (loading) {
        return <Loader />
    }

    if (error) {
        return (
            <AppError message={error} />
        );
    }

    return (
        <div className={classes.root}>
            <GridList className={classes.gridList}>
                {contestData.map((contest) => (
                    <>
                        <ContestDetails
                            key={contest.id}
                            {...contest}
                            image=
                            {<GridList key={contest.image}>
                                <img src={`http://localhost:3001/public/${contest.image}`} alt={contest.title} />
                            </GridList>}
                        />
                    </>
                ))}
            </GridList>
        </div>
    );
}

export default ContestsbyPhase;