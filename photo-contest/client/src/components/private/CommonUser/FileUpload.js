import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useParams } from 'react-router-dom';
import Loader from '../../Loader/Loader';



const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: 'none',
    },
}));
  
const FileUpload = () => {
    const classes = useStyles();
    const [file, setFile] = useState(null);
    const [loading, setLoading] = useState(false);
    const { id } = useParams();

    const upload = () => {
        if (file) {
            const formData = new FormData();
            formData.set('avatar', file);
            setLoading(true);
            fetch(`http://localhost:3001/users/${id}/avatar`, {
                method: 'POST',
                headers: { Authorization: `Bearer ${localStorage.getItem('token')}`},
                body: formData
            })
            .then(r => r.json())
            .then((data) => {
                console.log(data);
                alert('Photo changed...')
            })
            .finally(() => setLoading(false))
        }
    };
    if (loading) {
        return <Loader />
    }
    return (
       
            <div className={classes.root}>
      <input
        accept="image/*"
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
        onChange={e => setFile(e.target.files[0])}
      />
      <label htmlFor="contained-button-file">
        <Button variant="contained" color="primary" component="span" >
          Upload
        </Button>
            </label>
            <label>
        <Button onClick={upload} variant="contained" color="primary" component="span" >
          Change Avatar
        </Button>
      </label>
            </div>
      
    )
}

export default FileUpload;
