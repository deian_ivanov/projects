import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import useAuthorizedGETRequest from '../../../custom-hooks/useAuthorizedFetch';
import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { NavLink } from 'react-router-dom';
import CheckRank from '../../../common/CheckRank/CheckRank';


const preventDefault = (event) => {
  event.preventDefault();
}



const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  seeMore: {
    marginTop: theme.spacing(3),
  }
}));

const UsersTable = () => {
  
    const classes = useStyles();
    const users = useAuthorizedGETRequest('http://localhost:3001/users');
  
  if (users.isLoading) {
    return (
      <>
        <div className={classes.root} style={{margin:'0 auto'}}>
           <CircularProgress />
        </div>
      </>
    )
  }

  return (
    <React.Fragment>
      <Title>Accounts</Title>
     
        <Table size="small" >
          <TableHead>
            <TableRow>
              <TableCell>Create Date</TableCell>
              <TableCell>Rank</TableCell>
              <TableCell>Avatar</TableCell>
              <TableCell>Username</TableCell>
              <TableCell>Contests</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.data.map((row) => (
            
              <TableRow key={row.id}>
                <TableCell>{row.createTime}</TableCell>
                <TableCell>
                  <CheckRank score={row.score} roleid={row.roleid}/>
                </TableCell>
                <NavLink to={`/users/${row.id}`}>
                <TableCell><div className={classes.root}>
                  <Avatar alt="Avatar" src={`http://localhost:3001/public/${row.avatar}`} />
     
                  </div></TableCell>
                  </NavLink>
                <TableCell>{row.username}</TableCell>
                <TableCell>{row.contests}</TableCell>
               
              </TableRow>
             
            ))}
          </TableBody>
        </Table>
     
      <div className={classes.seeMore}>
        <Link color="primary" href="#" onClick={preventDefault}>
          See more accounts
        </Link>
          </div>
          
        
    </React.Fragment>
  );
}

export default UsersTable;