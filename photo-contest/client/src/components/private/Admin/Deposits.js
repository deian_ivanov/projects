import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Contest from '../../Contest/Contest';

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

const Deposits = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography color="textSecondary" className={classes.depositContext}>
        {new Date().toDateString()}
      </Typography>
      <div>
        <Contest />
      </div>
    </React.Fragment>
  );
}

export default Deposits;