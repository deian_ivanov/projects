import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import './CreateContest.css';
import moment from 'moment';
import { BASE_URL } from '../../../../common/constants';
import AppError from '../../../AppError/AppError'
import FileUpload from './UploadPicture';

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 250,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const AddressForm = () => {

  const contest = {
    title: '',
    category: '',
    description: '',
    is_open: 'invitational',
    date_1: moment().format(`YYYY-M-D`),
    date_2: moment().format(`YYYY-M-D`),
    date_3: moment().format(`YYYY-M-D`)
  };

  const classes = useStyles();
  const [error, setError] = React.useState(null);
  const [contestId, setContestId] = React.useState(0);

  const createContest = () => {
    const token = localStorage.getItem('token');

    if (!contest.title) {
      alert(`Enter a valid contest title!`);
      return;
    }

    // if (!contest.category) {
    //   alert(`Enter a valid contest category!`);
    //   return;
    // }

    // if (!contest.description) {
    //   alert(`Enter a valid contest description!`);
    //   return;
    // }
    if (!contest.is_open) {
      alert(`Enter a valid contest description!`);
      return;
    }

    if (!contest.date_1) {
      alert(`Date format should be YYYY-MM-DD!`);
      return;
    }

    if (!contest.date_2) {
      alert(`Date format should be YYYY-MM-DD!`);
      return;
    }

    if (!contest.date_3) {
      alert(`Date format should be YYYY-MM-DD!`);
      return;
    }

    fetch(`${BASE_URL}/contests`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(contest),
    })
      .then(response => response.json())
      .then(result => {
        if (result.error) {
          throw new Error(result.message);
        }
        setContestId(result.contest.id);
        alert('Upload a contest picture')
      })
      .catch(error => setError(error.message));
  };

  const updateContestProp = (prop, value) => {
    contest[prop] = value;
  };

  if (error) {
    return (
      <AppError message={error} />
    );
  }

  return (
    <React.Fragment>
      <div className="modal-view">
        <Typography variant="h6" gutterBottom>
          Create Contest
  </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="title"
              name="title"
              label="Title"
              fullWidth
              autoComplete="Title"
              onChange={(e) => updateContestProp('title', e.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              required
              id="category"
              name="category"
              label="Category"
              fullWidth
              autoComplete="Category"
              onChange={(e) => updateContestProp('category', e.target.value)}
            />
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="description"
                name="description"
                label="Description"
                fullWidth
                autoComplete="Description"
                onChange={(e) => updateContestProp('description', e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="type-native-simple">Type</InputLabel>
                <Select
                  native
                  onChange={(e) => updateContestProp('is_open', e.target.value)}
                >
                  <option aria-label="None" value="" />
                  <option value={'Open'}>Open</option>
                  <option value={'Invitational'}>Invitational</option>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="phase-one-deadline"
                // name="datetimes"
                label="Phase I deadline"
                type="date"
                defaultValue="2020-11-01"
                fullWidth
                autoComplete="date_1"
                onChange={(e) => updateContestProp('date_1', e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="phase-two-deadline"
                name="date_2"
                label="Phase II deadline"
                type="date"
                defaultValue="2020-12-05"
                fullWidth
                autoComplete="date_2"
                onChange={(e) => updateContestProp('date_2', e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="phase-three-deadline"
                name="date_3"
                label="Phase III deadline"
                type="date"
                defaultValue="2020-12-30"
                fullWidth
                autoComplete="date_3"
                onChange={(e) => updateContestProp('date_3', e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FileUpload contestId={contestId} />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                startIcon={<SaveIcon />}
                onClick={createContest}
              >
                Save
              </Button>
              {/* <br/>
              <br/> */}
            </Grid>
          </Grid>
        </Grid>
      </div>
    </React.Fragment>
  );
}
export default AddressForm;