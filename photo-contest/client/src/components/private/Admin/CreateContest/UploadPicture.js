import React, { useState } from 'react';
import { Button } from "@material-ui/core";
import { NavLink } from 'react-router-dom';

const FileUpload = (props) => {
    const [file, setFile] = useState(null);

    const upload = () => {
        const token = localStorage.getItem('token');

        if (file) {
            const formData = new FormData();
            formData.set('image', file);

            fetch(`http://localhost:3001/contests/${props.contestId}/picture`, {
                method: 'POST',
                headers: { Authorization: `Bearer ${token}` },
                body: formData
            })
                .then(result => result.json())
                .then((data) => {
                    console.log(data);
                })
        }
    };

    return (
        <form>
            <input type="file" onChange={e => setFile(e.target.files[0])} />
            <br />
            <br />
            <Button onClick={upload} variant="contained" color="primary" component="span" >
               <NavLink to={`/contests/${props.contestId}`}>Upload picture</NavLink>
            </Button>
        </form>
    )
}

export default FileUpload;
