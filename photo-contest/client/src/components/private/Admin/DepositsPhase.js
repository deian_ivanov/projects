import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ContestsByPhase from '../../Contest/ContestsByPhase';
import Contest from '../../Contest/Contest';

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

const DepositsPhase = ({ date }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography color="textSecondary" className={classes.depositContext}>
      </Typography>
      <div>
        <ContestsByPhase date={date} />
        {/* <Contest /> */}
      </div>
    </React.Fragment>
  );
}

export default DepositsPhase;