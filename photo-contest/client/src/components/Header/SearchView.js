import React from 'react';
import { useAuth } from '../../auth/authContext';
import useAuthorizedGETRequest from '../../custom-hooks/useAuthorizedFetch';
import useCustomQueryParams from '../../custom-hooks/useCustomQueryParams';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { NavLink } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';



const useStyles = makeStyles((theme) => ({
  root: {
   
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  media: {
    height: 200,
  },
}));

const SearchView = () => {

    const classes = useStyles();
    const { user } = useAuth();
    const { query } = useCustomQueryParams();
    const participants = useAuthorizedGETRequest(`http://localhost:3001/users`);
    const contests = useAuthorizedGETRequest(`http://localhost:3001/contests`);

    if (participants.isLoading || contests.isLoading) {
      return (
        <>
           <div className={classes.root}>
               <CircularProgress />
           </div>
        </>
      )
    }
   

    return (
        <> 
        {query && participants.data && contests.data && <div><h3>Search results:</h3></div>}    
        {user.role === 'Organizer' && query && participants.data 
                .filter(user => user.username.toLowerCase().includes(query.toLowerCase()))
                .map(users =>  
                  <>
                    <div className={classes.root}>
                    <Grid container spacing={3}>
                    <Grid item auto>
                    <Card className={classes.root}>
                    <NavLink to={`/users/${users.id}`}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={`http://localhost:3001/public/${users.avatar}`}
          title="Avatar"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
              Username: {users.username}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
              Points: { users.score }
          </Typography>
        </CardContent>
                        </CardActionArea>
                        </NavLink>
                        </Card>
                        </Grid>
                      </Grid>
                      </div>
                </>
        )}
        
        {query && contests.data 
                .filter(contest => contest.title.toLowerCase().includes(query.toLowerCase()))
                .map(contest =>  
                  <>
                    <div className={classes.root}>
                    <Grid container spacing={3}>
                    <Grid item auto>
                    <Card className={classes.root}>
                    <NavLink to={`/contests/${contest.id}`}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={`http://localhost:3001/public/${contest.image}`}
          title="Avatar"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
              Title: {contest.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
              Category: { contest.category }
          </Typography>
        </CardContent>
                        </CardActionArea>
                        </NavLink>
                        </Card>
                        </Grid>
                      </Grid>
                      </div>
                </>
                )}
  
    </>
  );
}

export default SearchView;