import { useAuth } from '../../auth/authContext';
import React, { useState } from 'react';
import {
  AppBar,
  Toolbar,
  List,
  Container,
  Button,
  ButtonGroup
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import useAuthorizedGETRequest from '../../custom-hooks/useAuthorizedFetch';
import CircularProgress from '@material-ui/core/CircularProgress';
import { fade } from '@material-ui/core/styles';
import Search from './Search';
import SearchView from './SearchView';
import Menu from '@material-ui/core/Menu';
import { NavLink } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import SvgIcon from '@material-ui/core/SvgIcon';
import './HeaderStyle.css';

const useStyles = makeStyles((theme) => ({

  navbarDisplayFlex: {
    display: `flex`,
    justifyContent: `space-between`
  },
  navDisplayFlex: {
    display: `flex`,
    justifyContent: `space-between`
  },
  linkText: {
    textDecoration: `none`,
    textTransform: `uppercase`,
    color: `white`
  },
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
}));

const HomeIcon = (props) => {
  return (
    <SvgIcon {...props}>
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>
  );
}

const Header = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const { user, setUser } = useAuth();
  const users = useAuthorizedGETRequest('http://localhost:3001/users');

  if (users.isLoading) {
    return (
      <>
        <div className={classes.root}>
          <CircularProgress />
        </div>
      </>
    )
  }
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    localStorage.removeItem('token');
    setUser(null);
  };
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Container maxWidth="sl" className={classes.navbarDisplayFlex}>
          <Link to='/home'> <HomeIcon className="home-icon" color="disabled" fontSize="large"  /></Link>
            <List>
              <div className="header-right">
                <ButtonGroup size="small" color="primary" aria-label="small outlined primary button group">
                  {user && <Button variant="contained" color="default" style={{ height: "31px", margin: "10px 10px" }}><Link className='nav-home' to='/dashboard'>Dashboard</Link></Button>}
                  {user && <Search />}
                  {!user && <Button variant="contained" color="default" style={{ marginTop: "10px" }}><Link className='nav-reg' to='/register'>Register</Link></Button>}
                  {!user && <Button variant="contained" color="default" style={{ marginTop: "10px" }}><Link className='nav-login' to='/login'>Login</Link></Button>}
                  {user && <div> <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                    <div className={classes.root} style={{ height: "44px" }}>
                      <Avatar key={users.data.id} alt="Avatar" src={`http://localhost:3001/public/${users.data.find((u)=>u.id === user.sub).avatar}`} style={{ margin: "4px" }} />
                      <p className="welcome-text" style={{ height: "31px", marginTop: "10px" }}>Hello, {user.username.toUpperCase()}</p>
                    </div>
                  </Button>
                    <Menu
                      id="simple-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleClose}
                    >
                      {user.role === 'Organizer' && <MenuItem onClick={handleClose}><NavLink to={`/users/${user.sub}`}>My account</NavLink></MenuItem>}
                      {user.role === 'Junky' && <MenuItem onClick={handleClose}><NavLink to={`/users/${user.sub}/me`}>My account</NavLink></MenuItem>}
                      <MenuItem onClick={handleClose}>{user && <Button variant="contained" color="default" onClick={logout} style={{ height: "31px" }}><Link className='nav-logut' to='/home'>Logout</Link></Button>}</MenuItem>
                    </Menu>
                  </div>
                  }
                </ButtonGroup>
              </div>
            </List>
          </Container>
        </Toolbar>
      </AppBar>
      {user && <SearchView />}
    </>
  );
}

export default Header;