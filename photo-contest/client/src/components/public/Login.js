import React, { useState } from 'react';
import { useAuth } from '../../auth/authContext';
import decode from 'jwt-decode';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import './Login.css';


    const required = (value) => value.trim().length >= 1;
    const minLen = (len) => (value) => value.trim().length >= len;
    const maxLen = (len) => (value) => value.trim().length <= len;
    const regex = (pattern) => (value) => pattern.test(value);

    const Login = () => {
    const { user, setUser } = useAuth();
 
      
      const [usernameControl, setUsernameControl] = useState({
        value: '',
        valid: true,
        validators: [required, minLen(5), maxLen(20)],
      });
      const [passwordControl, setPasswordControl] = useState({
        value: '',
        valid: true,
        validators: [required, regex(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,30}$/)],
      });

      const validate = (value, validators) =>
        validators.every((validator) => validator(value));

      const onInputChange = (ev) => {
        const {
          value,
          name
        } = ev.target;

        if (name === 'username') {
          const copyControl = {
            ...usernameControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, usernameControl.validators);
          setUsernameControl(copyControl);
        } else if (name === 'password') {
          const copyControl = {
            ...passwordControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, passwordControl.validators);
          setPasswordControl(copyControl);
        }
      };

        const handleSubmit = (ev) => {
            ev.preventDefault();

            const user = {
                username: usernameControl.value,
                password: passwordControl.value,
         
            };

            fetch('http://localhost:3001/auth/signin', {
                method: 'POST',
                body: JSON.stringify(user),
                headers: {
                    'content-type': 'application/json',
                },
            })
                .then(r => r.json())
                .then((data) => {
                    if (data.message) {
                        alert(data.message);
                    } else {
                        localStorage.setItem('token', data.token);
                        const user = decode(data.token);
                        setUser(user);
                  
                    }
                })
        };

          
            

      const Copyright = () => {
        return (
          <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
              Denitsa Kireva and Deyan Ivanov
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
          </Typography>
        );
      }

      const useStyles = makeStyles((theme) => ({
        paper: {
          marginTop: theme.spacing(8),
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        },
        avatar: {
          margin: theme.spacing(1),
          backgroundColor: theme.palette.secondary.main,
        },
        form: {
          width: '100%', 
          marginTop: theme.spacing(3),
        },
        submit: {
          margin: theme.spacing(3, 0, 2),
        },
      }));
      
      const classes = useStyles();
      return ( 
      <>
    <div className="login-bg">
          <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
                    <TextField
                      name="username"
                      id="username"
                      value={usernameControl.value}
                      onChange={onInputChange}
                      autoComplete="fname"
                      variant="outlined"
                      required
                      fullWidth
                      label="Enter your username"
                      autoFocus
                    />
                    {!usernameControl.valid && <div style={{color:"red"}}>The name should be between 5 and 20 characters!</div>}
            </Grid>
            <Grid item xs={12}>
                    <TextField
                      name="password"
                      value={passwordControl.value}
                      onChange={onInputChange}
                      variant="outlined"
                      required
                      fullWidth
                      label="Password"
                      type="password"
                      id="password"
                      autoComplete="current-password"
                    />
                    {!passwordControl.valid && <div style={{color:"red"}}>The password should contain letters and at least one number!</div>}
            </Grid>
          </Grid>
                <Button
                  disabled={!usernameControl.valid || !passwordControl.valid}
                  onClick={handleSubmit}
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
          >
            Login
          </Button>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
            </Container>
            </div>
        </>
      );
};
    



    export default Login;