import React, { useState } from 'react';
import './Register.css';
import { useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';


    const required = (value) => value.trim().length >= 1;
    const minLen = (len) => (value) => value.trim().length >= len;
    const maxLen = (len) => (value) => value.trim().length <= len;
    const regex = (pattern) => (value) => pattern.test(value);

    const Register = () => {

      const history = useHistory();
      
      const [usernameControl, setUsernameControl] = useState({
        value: '',
        valid: true,
        validators: [required, minLen(5), maxLen(20)],
      });
      const [firstNameControl, setFirstNameControl] = useState({
        value: '',
        valid: true,
        validators: [required, minLen(3), maxLen(20)],
      });
      const [secondNameControl, setSecondNameControl] = useState({
        value: '',
        valid: true,
        validators: [required, minLen(3), maxLen(20)],
      });
      const [passwordControl, setPasswordControl] = useState({
        value: '',
        valid: true,
        validators: [required, regex(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,30}$/)],
      });

      const [emailControl, setEmailControl] = useState({
        value: '',
        valid: true,
        validators: [required, regex(/^\S+@\S+$/)],
      });

      const validate = (value, validators) =>
        validators.every((validator) => validator(value));

      const onInputChange = (ev) => {
        const {
          value,
          name
        } = ev.target;

        if (name === 'username') {
          const copyControl = {
            ...usernameControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, usernameControl.validators);
          setUsernameControl(copyControl);
        }else if (name === 'firstName') {
          const copyControl = {
            ...firstNameControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, firstNameControl.validators);
          setFirstNameControl(copyControl);
        }else if (name === 'secondName') {
          const copyControl = {
            ...secondNameControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, secondNameControl.validators);
          setSecondNameControl(copyControl);
        } else if (name === 'password') {
          const copyControl = {
            ...passwordControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, passwordControl.validators);
          setPasswordControl(copyControl);
        }else if (name === 'email') {
          const copyControl = {
            ...emailControl
          };
          copyControl.value = value;
          copyControl.valid = validate(value, emailControl.validators);
          setEmailControl(copyControl);
        }
      };

      const handleSubmit = (ev) => {
        ev.preventDefault();

        const user = {
          username: usernameControl.value,
          firstName: firstNameControl.value,
          secondName: secondNameControl.value,
          password: passwordControl.value,
          email: emailControl.value
        };

        fetch('http://localhost:3001/users', {
            method: 'POST',
            body: JSON.stringify(user),
            headers: {
              'content-type': 'application/json',
            },
          })
          .then((res) => res.json())
          .then((data) => {
            history.push('/login');
          })
          .catch(console.log);
      };

      const Copyright = () => {
        return (
          <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
              Denitsa Kireva and Deyan Ivanov
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
          </Typography>
        );
      }

      const useStyles = makeStyles((theme) => ({
        paper: {
          marginTop: theme.spacing(8),
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        },
        avatar: {
          margin: theme.spacing(1),
          backgroundColor: theme.palette.secondary.main,
        },
        form: {
          width: '100%', 
          marginTop: theme.spacing(3),
        },
        submit: {
          margin: theme.spacing(3, 0, 2),
        },
      }));
      
      const classes = useStyles();
      return ( 
      <>
    <div className="register-bg">
          <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
                    <TextField
                      name="username"
                      id="username"
                      className="fullname"
                      value={usernameControl.value}
                      onChange={onInputChange}
                      autoComplete="fname"
                      variant="outlined"
                      required
                      fullWidth
                      label="Enter your username"
                      autoFocus
                    />
                    {!usernameControl.valid && <div style={{color:"red"}}>The username should be between 5 and 20 characters!</div>}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      name="firstName"
                      id="firstname"
                      className="fullname"
                      value={firstNameControl.value}
                      onChange={onInputChange}
                      autoComplete="fname"
                      variant="outlined"
                      required
                      fullWidth
                      label="Enter your First Name"
                      autoFocus
                    />
                    {!firstNameControl.valid && <div style={{color:"red"}}>First name should be between 3 and 20 characters!</div>}
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      name="secondName"
                      id="secondName"
                      className="fullname"
                      value={secondNameControl.value}
                      onChange={onInputChange}
                      autoComplete="fname"
                      variant="outlined"
                      required
                      fullWidth
                      label="Enter your Second Name"
                      autoFocus
                    />
                    {!secondNameControl.valid && <div style={{color:"red"}}>Second name should be between 3 and 20 characters!</div>}
            </Grid>
            <Grid item xs={12}>
                    <TextField
                      name="password"
                      className="fullname"
                      value={passwordControl.value}
                      onChange={onInputChange}
                      variant="outlined"
                      required
                      fullWidth
                      label="Password"
                      type="password"
                      id="password"
                      autoComplete="current-password"
                    />
                    {!passwordControl.valid && <div style={{color:"red"}}>The password should contain letters and at least one number!</div>}
            </Grid>
            <Grid item xs={12}>
                    <TextField
                      name="email"
                      type="email"
                      value={emailControl.value}
                      onChange={onInputChange}
                      variant="outlined"
                      required
                      fullWidth
                      id="email"
                      label="Email Address"
                      autoComplete="email"
                    />
                    {!emailControl.valid && <div style={{color:"red"}}>Please enter a valid email!</div>}
            </Grid>
          </Grid>
                <Button
                  disabled={!usernameControl.valid || !passwordControl.valid}
                  onClick={handleSubmit}
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
          >
            Register
          </Button>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
            </Container>
            </div>
        </>
      );
};
    



    export default Register;