import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <>
    
    <footer className="site-footer">
      <div className="container">
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <h6>About</h6>
            <p className="text-justify">The Photo contest <i>One project by Denitsa Kireva and Deyan Ivanov</i> </p>
          </div>

          <div className="col-xs-6 col-md-3">
            <h6>Categories</h6>
            <ul className="footer-links">
              <li>Animals</li>
              <li>Buildings</li>
              <li>Nature</li>
              <li>People</li>
            </ul>
          </div>

          <div className="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul className="footer-links">
              <li>About Us</li>
              <li>Contact Us</li>
            </ul>
          </div>
        </div>
     
      </div>
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-sm-6 col-xs-12">
            <p className="copyright-text">Copyright &copy; 2020 
            </p>
          </div>
        </div>
      </div>
</footer>
        </>
    )
}

export default Footer;