import React from 'react';
import Footer from './Footer/Footer';
import Header from './Header/Header';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './PublicApp.css';
import Register from './public/Register';
import Login from './public/Login';
import HomePage from './public/Home/HomePage';


const PublicApp = () => {

    return (
        <>

            <BrowserRouter>
                <Header />
                <Switch>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/register">
                        <Register />
                    </Route>
                    <Route path="*">
                        <HomePage />
                    </Route>
                </Switch>
            </BrowserRouter>

            <Footer />
        </>
    )
}

export default PublicApp;