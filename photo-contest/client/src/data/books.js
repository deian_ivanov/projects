const booksData = [
   { 
      id: 1,
      title: "My new book",
      author: "Author 1",
      isDeleted: false,
      isAvailable: true
   },
   { 
      id: 2,
      title: "My old book",
      author: "Author 2",
      isDeleted: false,
      isAvailable: true
   },
   { 
      id: 3,
      title: "Best seller",
      author: "Author 3",
      isDeleted: true,
      isAvailable: false
   },
];

export default booksData;