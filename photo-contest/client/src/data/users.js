const users = [
    {
        id: 1,
        firstName: "User_1",
        lastName: "Petrov",
        role: "admin",
        username: "userOne",
        password: "userPassOne",
        email: "kireva@tu-sofia.bg"

    },
    {
        id: 1,
        firstName: "User_2",
        lastName: "Ivanov",
        role: "publicUser",
        username: "userTwo",
        password: "userPassTwo",
        email: "kireva@tu-sofia.bg"
    },
    {
        id: 1,
        firstName: "User_1",
        lastName: "Petrov",
        role: "privateUser",
        username: "userThree",
        password: "userPassThree",
        email: "kireva@tu-sofia.bg"
    },
    
];

export default users;