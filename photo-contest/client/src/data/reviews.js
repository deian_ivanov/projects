const reviewsData = [
    {
        id: 1,
        text: "Nice book!",
        user: "Ivan Petrov",
        isDeleted: false,
        bookId: 2,
        date: new Date(),
        likes: 4

    },
    {
        id: 2,
        text: "Very good!",
        user: "Ivan Petrov",
        isDeleted: false,
        bookId: 3,
        date: new Date(),
        likes: 2
    },
    {
        id: 3,
        text: "Not good!",
        user: "Ivan Petrov",
        isDeleted: false,
        bookId: 1,
        date: new Date(),
        likes: 0
    },
    {
        id: 4,
        text: "Beautiful book!",
        user: "Petar Ivanov",
        isDeleted: false,
        bookId: 3,
        date: new Date(),
        likes: 45
    },
    {
        id: 5,
        text: "Nice, nice book!",
        user: "Emil Dimitrov",
        isDeleted: false,
        bookId: 2,
        date: new Date(),
        likes: 10
    },
];

export default reviewsData;