import { useLocation } from 'react-router-dom';

const useCustomQueryParams = () =>
    Object.fromEntries(new URLSearchParams(useLocation().search));

export default useCustomQueryParams;