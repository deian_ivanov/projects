import { useState, useEffect } from 'react';


const useAuthorizedGETRequest = (url) => {
    const [response, setResponse] = useState({ isLoading: true, data: null, error: null });
    const token = localStorage.getItem('token');

    useEffect(() => {
        setResponse({ isLoading: true, data: null, error: null });

        fetch(url, {
            method: 'GET',
            headers: { Authorization: `Bearer ${token}` }
        })
            .then(r => {
                if (r.status < 400) {
                    return r.json()
                } else {
                    throw new Error(r.status)
                }
            })
            .then(res => {
                setResponse({ isLoading: false, data: res, error: null })
            })
            .catch(error => {
                setResponse({ isLoading: false, data: null, error: error })
            })
    }, [url, token]);

    return response;
};

export default useAuthorizedGETRequest;
