import './App.css';
import { useState } from 'react';
import AuthContext from './auth/authContext';
import decode from 'jwt-decode';
import PrivateApp from './components/PrivateApp';
import PublicApp from './components/PublicApp';
import React from 'react';
import 'fontsource-roboto';


const App = () => {
    const token = localStorage.getItem('token');
    const [user, setUser] = useState(token 
        ? decode(token) 
        : null);
 
    return (
        <>
       

        <div className="main">
            <AuthContext.Provider value={{ user: user, setUser: setUser }}>                
                {user ? <PrivateApp /> : <PublicApp />}
            </AuthContext.Provider>
        </div>
     
            </>
    );
}

export default App;

